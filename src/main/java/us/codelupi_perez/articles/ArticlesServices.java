package us.codelupi_perez.articles;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArticlesServices {

    private ArticleRepository articles;

    public ArticlesServices(ArticleRepository articles){
        this.articles = articles;
    }

    public List<Article> findAll(){
        return articles.findAll();
    }

    //Create a new article if not already existing else return the one that already exist
    public Article add(Article article){return isArticleExist(article.getName())? findArticleByName(article.getName()) : articles.save(article);}

    public boolean isArticleExist(String articlename){ return articles.existsArticleByName(articlename);}

    public boolean isArticleExist(Long articleID){return articles.existsArticleById(articleID);}

    public Article findArticle(Long articleID) {return articles.findArticleById(articleID);}

    private Article findArticleByName(String articleName) {return articles.findArticleByName(articleName);}

    public Article newArticle() { return articles.save(new Article().withName("Nouveau Article"));
    }

    public void saveArticle(Article article) {
        articles.save(article);
    }

    @Transactional
    public void deleteArticle(Long articleID) {
        articles.removeArticleById(articleID);
    }

}
