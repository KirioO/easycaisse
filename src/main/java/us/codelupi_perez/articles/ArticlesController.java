package us.codelupi_perez.articles;

import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
@RequestMapping("/articles")
public class ArticlesController {

    private ArticlesServices articles;

    public ArticlesController(ArticlesServices articles){
        this.articles = articles;
    }

    @RequestMapping(path ="", method = GET)
    public List<Article> articles(){
        return articles.findAll();
    }

    @RequestMapping(path = "/{articleID}", method = GET)
    public Article getArticle(@PathVariable Long articleID){
        return articles.findArticle(articleID);
    }

    @RequestMapping(path = "/new-article", method = POST)
    public Article newArticle() {
       return articles.newArticle();
    }

    @RequestMapping (path = "/update", method = PUT)
    public boolean updateArticle(@RequestBody Article article){

        if( articles.isArticleExist(article.getId())){
            articles.saveArticle(article);
            return true;
        }
        else{
            return false;
        }
    }


    @RequestMapping(path = "/{articleID}", method = DELETE)
    public boolean deleteArticle(@PathVariable Long articleID) {

        if (articles.isArticleExist(articleID)) {
            articles.deleteArticle(articleID);
            return true;
        }
        else{
            return false;
        }
    }

}
