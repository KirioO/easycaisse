package us.codelupi_perez.articles;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Long> {

    List<Article> findAll();

    boolean existsArticleById(Long id);
    boolean existsArticleByName(String name);

    Article findArticleById(Long id);
    Article findArticleByName(String name);

    void removeArticleById(Long articleID);


}
