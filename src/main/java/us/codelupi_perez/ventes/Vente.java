package us.codelupi_perez.ventes;

import us.codelupi_perez.articles.Article;
import us.codelupi_perez.event.Event;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Vente {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    private List<Article> articlesList;

    @ManyToOne
    private Event event;

    private LocalDate date;

    public void addArticle(Article article){ articlesList.add(article);}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Article> getArticlesList() {
        return articlesList;
    }

    public void setArticlesList(List<Article> articlesList) {
        this.articlesList = articlesList;
    }

    public void removeArticle(Article article) {
        articlesList = articlesList.stream().filter(Larticle -> !Larticle.equals(article)).collect(Collectors.toList());
    }
}
