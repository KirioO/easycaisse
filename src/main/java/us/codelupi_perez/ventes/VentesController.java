package us.codelupi_perez.ventes;

import org.springframework.web.bind.annotation.*;
import us.codelupi_perez.articles.ArticlesServices;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/ventes")
public class VentesController {

    private VentesServices ventes;
    private ArticlesServices articles;

    public VentesController(VentesServices ventes, ArticlesServices articles){
        this.ventes = ventes;
        this.articles = articles;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Vente> ventes(){
        return ventes.findAll();
    }

    @RequestMapping(path = "/new-vente", method = RequestMethod.POST)
    public Vente newVente(){
        return ventes.newVente();
    }

    @RequestMapping(path = "/delete-vente/{venteID}", method = RequestMethod.DELETE)
    public boolean deleteVente(@PathVariable Long venteID){

        if(!ventes.isExisting(venteID)){
            ventes.deleteVente(venteID);
            return false;
        }
        else{
            return true;
        }
    }

    @RequestMapping(path = "{venteId}/add-article/{articleID}", method = RequestMethod.POST)
    public boolean addArticleToVente(@PathVariable Long venteId, @PathVariable Long articleID){

        if(articles.isArticleExist(articleID)){
            ventes.addArticleToVente(articles.findArticle(articleID), venteId);
            return true;
        }
        else{
            return false;
        }
    }

    @RequestMapping(path = "{venteId}/remove-article/{articleID}", method = RequestMethod.DELETE)
    public boolean removeArticleFromVente(@PathVariable Long venteId, @PathVariable Long articleID){

        if(articles.isArticleExist(articleID)){
            ventes.removeArticleFromVente(articles.findArticle(articleID), venteId);
            return true;
        }
        else{
            return false;
        }
    }

}
