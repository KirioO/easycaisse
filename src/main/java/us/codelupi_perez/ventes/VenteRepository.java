package us.codelupi_perez.ventes;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VenteRepository extends CrudRepository<Vente, Long> {

    List<Vente> findAll();

    List<Vente> findVentesByEvent_Id(Long eventId);

    Vente findVenteById(Long venteId);

    boolean existsById(Long venteId);
}
