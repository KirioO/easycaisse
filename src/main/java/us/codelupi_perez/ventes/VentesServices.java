package us.codelupi_perez.ventes;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.codelupi_perez.articles.Article;

import java.util.List;

@Service
public class VentesServices {

    private VenteRepository ventes;

    public VentesServices(VenteRepository ventes){this.ventes = ventes;}

    public List<Vente> findAll(){ return ventes.findAll();}

    public List<Vente> findVentesOfEvent(Long eventId){return ventes.findVentesByEvent_Id(eventId);}

    public Vente findVenteById(Long venteId){return ventes.findVenteById(venteId);}

    public void addArticleToVente(Article article, Long venteId){
        Vente vente = ventes.findVenteById(venteId);
        vente.addArticle(article);
        ventes.save(vente);
    }

    public Vente newVente() {
        return ventes.save(new Vente());
    }

    public boolean isExisting(Long venteId){return ventes.existsById(venteId);}

    public void removeArticleFromVente(Article article, Long venteId) {
        Vente vente = ventes.findVenteById(venteId);
        vente.removeArticle(article);
        ventes.save(vente);
    }

    @Transactional
    public void deleteVente(Long venteID) {
        ventes.deleteById(venteID);
    }
}
