package us.codelupi_perez.catalogue;

import us.codelupi_perez.articles.Article;
import us.codelupi_perez.prix.Prix;


import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Catalogue {
    @Id
    @GeneratedValue ( strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany (cascade = CascadeType.PERSIST)
    private List<Article> listarticles;

    @ManyToMany (cascade = CascadeType.PERSIST)
    private List<Prix> listprix;

    public void addArticle(Article a){
        this.listarticles.add(a);
    }

    public void removeArticle(Article article) {
        listarticles = listarticles.stream().filter(Larticle -> !Larticle.equals(article)).collect(Collectors.toList());
        //When we remove an article with must remove the price too
        listprix = listprix.stream().filter(Lprix -> !Lprix.getArticle().equals(article)).collect(Collectors.toList());
    }

    public void addPrix(Prix prix ){
        //avoid doublon
        listprix = listprix.stream().filter(Lprix -> !Lprix.getArticle().equals(prix.getArticle())).collect(Collectors.toList());
        this.listprix.add(prix);
    }

    public void removePrix(Prix prix) {
        listprix = listprix.stream().filter(Lprix -> !Lprix.getArticle().equals(prix.getArticle())).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Article> getListarticles() {
        return listarticles;
    }

    public void setListarticles(List<Article> listarticles) {
        this.listarticles = listarticles;
    }

    public List<Prix> getListprix() {
        return listprix;
    }

    public void setListprix(List<Prix> listprix) {
        this.listprix = listprix;
    }


    public boolean containsArticle(Article article) {
        return listarticles.contains(article);
    }
}
