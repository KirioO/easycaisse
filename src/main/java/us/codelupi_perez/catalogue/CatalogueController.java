package us.codelupi_perez.catalogue;

import org.springframework.web.bind.annotation.*;
import us.codelupi_perez.articles.Article;
import us.codelupi_perez.articles.ArticlesServices;
import us.codelupi_perez.prix.PrixesServices;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/catalogues")
public class CatalogueController {

    private CataloguesServices catalogues;
    private ArticlesServices articles;
    private PrixesServices prixes;

    public CatalogueController(CataloguesServices catalogues, ArticlesServices articles, PrixesServices prixes) {
        this.catalogues = catalogues;
        this.articles = articles;
        this.prixes = prixes;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Catalogue> catalogues(){return catalogues.findAll();}

    @RequestMapping(path = "/{catalogueID}", method = RequestMethod.GET)
    public Catalogue getCatalogue(@PathVariable Long catalogueID){return catalogues.FindCatalogueByID(catalogueID);}

    @RequestMapping(path = "/new_catalogue", method = RequestMethod.POST)
    public Catalogue newCatalogue(){
        return catalogues.newCatalogue();
    }

    @RequestMapping(path = "{catalogueID}/add-article/{articleID}", method = RequestMethod.POST)
    public boolean catalogues_add_article(@PathVariable Long catalogueID, @PathVariable Long articleID){
        if(!articles.isArticleExist(articleID)){
            return false;
        }

        catalogues.AddArticleToCatalogue(articles.findArticle(articleID), catalogueID);
        return true;
    }

    @RequestMapping(path = "{catalogueID}/article/{articleID}/add-price/{price}", method = RequestMethod.POST)
    public boolean catalogue_add_price(@PathVariable Long catalogueID, @PathVariable Long articleID, @PathVariable Float price){

        if(!articles.isArticleExist(articleID)){
            return false;
        }
        else{
            Article article =  articles.findArticle(articleID);
            if (!catalogues.isArticleInCatalogue(article, catalogueID)){
                if(!prixes.alreadyExist(price, articleID)){
                    catalogues.AddPrixToCatalogue(prixes.addNewPrice(price, article), catalogueID);
                }
                else{
                    catalogues.AddPrixToCatalogue(prixes.findPrix(price, articleID), catalogueID);
                }
            }
            else{
                return false;
            }
        }
        return true;
    }

    @RequestMapping(path = "{catalogueID}/article/{articleID}", method = RequestMethod.DELETE)
    public boolean deleteArticleFromCatalogue(@PathVariable Long catalogueID, @PathVariable Long articleID){

        if(articles.isArticleExist(articleID)){
            catalogues.removeArticleFromCatalogue(catalogueID, articles.findArticle(articleID));
        }
        else{
            return false;
        }
        return true;
    }

    @RequestMapping(path = "{catalogueID}/prix/{prixID}", method = RequestMethod.DELETE)
    public boolean deletePrixFromCatalogue(@PathVariable Long catalogueID, @PathVariable Long prixID){

        if(prixes.alreadyExist(prixID)){
            catalogues.removePriceFromCatalogue(catalogueID, prixes.findPrix(prixID));
        }
        else{
            return false;
        }
        return true;
    }

    @RequestMapping(path = "{catalogueID}", method = RequestMethod.DELETE)
    public boolean deleteCatalogue(@PathVariable Long catalogueID){

        if(catalogues.isExisting(catalogueID)){
            catalogues.deleteCatalogue(catalogueID);
        }
        else{
            return false;
        }
        return true;
    }

}
