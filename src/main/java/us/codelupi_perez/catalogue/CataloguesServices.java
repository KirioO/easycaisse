package us.codelupi_perez.catalogue;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.codelupi_perez.articles.Article;
import us.codelupi_perez.prix.Prix;

import java.util.List;

@Service
public class CataloguesServices {

    private CatalogueRepository catalogues;

    public CataloguesServices(CatalogueRepository catalogues){
        this.catalogues = catalogues;
    }

    public Catalogue newCatalogue() {
        return catalogues.save(new Catalogue());
    }

    public List<Catalogue> findAll(){
        return catalogues.findAll();
    }

    public void AddArticleToCatalogue(Article article, Long catalogueID){
        Catalogue catalogue = FindCatalogueByID(catalogueID);
        catalogue.addArticle(article);
        catalogues.save(catalogue);
    }

    public void AddPrixToCatalogue(Prix prix, Long catalogueID){
        Catalogue catalogue = FindCatalogueByID(catalogueID);
        catalogue.addPrix(prix);
        catalogues.save(catalogue);
    }

    public Catalogue FindCatalogueByID(Long ID){
        return catalogues.findCatalogueById(ID);
    }

    public boolean isExisting(Long id){return catalogues.existsCatalogueById(id);}

    public boolean isArticleInCatalogue(Article article, Long catalogueID) {
        Catalogue catalogue = FindCatalogueByID(catalogueID);
        return catalogue.containsArticle(article);
    }


    public void removeArticleFromCatalogue(Long catalogueID, Article article){
        Catalogue catalogue = FindCatalogueByID(catalogueID);
        catalogue.removeArticle(article);
        catalogues.save(catalogue);
    }
    public void removePriceFromCatalogue(Long catalogueID, Prix prix){
        Catalogue catalogue = FindCatalogueByID(catalogueID);
        catalogue.removePrix(prix);
        catalogues.save(catalogue);
    }

    @Transactional
    public void deleteCatalogue(Long catalogueID) {
        catalogues.deleteCatalogueById(catalogueID);
    }
}
