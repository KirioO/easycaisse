package us.codelupi_perez.catalogue;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatalogueRepository extends CrudRepository<Catalogue, Long> {

    List<Catalogue> findAll();

    Catalogue findCatalogueById(Long id);

    boolean existsCatalogueById(Long id);

    void deleteCatalogueById(Long id);

}
