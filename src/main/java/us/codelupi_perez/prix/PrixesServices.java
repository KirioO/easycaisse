package us.codelupi_perez.prix;

import org.springframework.stereotype.Service;
import us.codelupi_perez.articles.Article;

import java.util.List;

@Service
public class PrixesServices {

    private PrixRepository prixes;

    public PrixesServices(PrixRepository prixes){this.prixes = prixes;}

    public List<Prix> findAll(){return prixes.findAll();}

    public Prix add(Prix prix){ return prixes.save(prix);}

    public boolean alreadyExist(Float price, Long articleID){return prixes.existsPrixByPriceAndArticle_Id(price, articleID);}

    public boolean alreadyExist(Long priceId){return prixes.existsPrixById(priceId);}

    public Prix findPrix(Float price, Long articleID){ return prixes.getPrixByPriceAndArticle_Id(price, articleID);}

    public Prix addNewPrice(Float price, Article article) {
        return prixes.save(new Prix().withPrice(price).withArticle(article));
    }

    public List<Prix> findArticlePrices(Long articleID){ return  prixes.getPrixesByArticle_Id(articleID);}

    public Prix findPrix(Long priceID) {
        return prixes.getPrixById(priceID);
    }

    public void removePrice(Long priceId){
        prixes.removePrixById(priceId);
    }
}
