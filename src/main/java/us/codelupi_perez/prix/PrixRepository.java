package us.codelupi_perez.prix;

import org.springframework.data.repository.CrudRepository;
import us.codelupi_perez.articles.Article;

import java.util.List;

public interface PrixRepository  extends CrudRepository<Prix, Long> {

    List<Prix> findAll();

    boolean existsPrixById(Long Id);
    boolean deletePrixById(Long Id);
    boolean existsPrixByPriceAndArticle_Id(Float price, Long articleID);

    Prix getPrixById(Long Id);
    Prix getPrixByPriceAndArticle_Id(Float price, Long ArticleId);

    List<Prix> getPrixesByArticle_Id(Long articleId);

    void removePrixById(Long prixId);
}
