package us.codelupi_perez.prix;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/prices")
public class PrixController {

    private PrixesServices prixes;

    public PrixController(PrixesServices prixes){
        this.prixes = prixes;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Prix> prices(){
        return prixes.findAll();
    }

    @RequestMapping(value = "/{priceID}", method = RequestMethod.GET)
    public Prix articlesPrices(@PathVariable Long priceID){
        return prixes.findPrix(priceID);
    }

    @RequestMapping(value = "/article/{articleID}", method = RequestMethod.GET)
    public List<Prix> getPrixForArticle(@PathVariable Long articleID){
        return prixes.findArticlePrices(articleID);
    }
}
