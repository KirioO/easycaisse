package us.codelupi_perez.prix;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import us.codelupi_perez.articles.Article;

import javax.persistence.*;

@Entity
public class Prix {
    private Float price;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Article article;

    public Prix withPrice(Float price){
        this.price = price;
        return this;
    }

    public Prix withArticle(Article article) {
        this.article = article;
        return this;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }


    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
