package us.codelupi_perez.event;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.codelupi_perez.catalogue.Catalogue;
import us.codelupi_perez.ventes.Vente;

import java.time.LocalDate;
import java.util.List;

@Service
public class EventServices {

    private EventRepository events;

    public EventServices(EventRepository events){this.events = events;}

    public Event newEvent() {
        return events.save(new Event().withStartDate(LocalDate.now()).withEndDate(LocalDate.now().plusDays(1)).withName("Nouvel Evenement"));
    }

    public void saveEvent(Event event) { events.save(event);}

    public List<Event> findAll(){return this.events.findAll();}

    public Event findEvent(Long eventID){return events.findEventById(eventID);}

    public void addCatalogueToEvent(Catalogue catalogue, Long eventID){
        Event event = events.findEventById(eventID);
        event.setCatalogue(catalogue);
        events.save(event);
    }

    public void addVenteToEvent(Vente vente, Long eventID){
        Event event = events.findEventById(eventID);
        event.addVente(vente);
        events.save(event);
    }

    public boolean isExisting(Long eventid){
        return events.existsEventById(eventid);
    }

    @Transactional
    public void deleteEvent(Long eventID) {
        events.deleteById(eventID);
    }
}
