package us.codelupi_perez.event;

import org.springframework.data.repository.CrudRepository;
import us.codelupi_perez.ventes.Vente;

import java.util.List;

public interface EventRepository extends CrudRepository<Event, Long> {

    List<Event> findAll();

    Event findEventById(Long id);

    boolean existsEventById(Long id);

    void deleteEventById(Long id);
}
