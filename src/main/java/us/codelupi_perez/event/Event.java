package us.codelupi_perez.event;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import us.codelupi_perez.catalogue.Catalogue;
import us.codelupi_perez.ventes.Vente;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "EVENEMENT")
public class Event {

    private LocalDate debut;

    private LocalDate fin;

    private String name;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Catalogue catalogue;

    @OneToMany (orphanRemoval = true)
    private List<Vente> ventes;

    public void addVente(Vente vente){
        ventes.add(vente);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Catalogue getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(Catalogue catalogue) {
        this.catalogue = catalogue;
    }

    public LocalDate getDebut() {
        return debut;
    }

    public void setDebut(LocalDate debut) {
        this.debut = debut;
    }

    public LocalDate getFin() {
        return fin;
    }

    public void setFin(LocalDate fin) {
        this.fin = fin;
    }

    public List<Vente> getVentes() {
        return ventes;
    }

    public void setVentes(List<Vente> ventes) {
        this.ventes = ventes;
    }

    public Event withStartDate(LocalDate startDate) {
        this.debut = startDate;
        return this;
    }

    public Event withEndDate(LocalDate endDate) {
        this.fin = endDate;
        return this;
    }

    public Event withName(String name){
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
