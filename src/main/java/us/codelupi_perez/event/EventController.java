package us.codelupi_perez.event;

import org.springframework.web.bind.annotation.*;
import us.codelupi_perez.catalogue.Catalogue;
import us.codelupi_perez.catalogue.CataloguesServices;
import us.codelupi_perez.ventes.Vente;
import us.codelupi_perez.ventes.VentesServices;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/events")
public class EventController {

    private EventServices events;
    private CataloguesServices catalogues;
    private VentesServices ventes;

    public EventController(EventServices events, CataloguesServices catalogues, VentesServices ventes){
        this.events = events;
        this.catalogues = catalogues;
        this.ventes = ventes;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Event> events(){
        return events.findAll();
    }

    @RequestMapping(path = "/new-event", method = RequestMethod.POST)
    public Event newEvent(){
        Event event = events.newEvent();
        events.addCatalogueToEvent(catalogues.newCatalogue(), event.getId());
        return event;
    }

    @RequestMapping(path = "/{eventID}", method = RequestMethod.GET)
    public Event getEvent(@PathVariable Long eventID){
        return events.findEvent(eventID);
    }

    @RequestMapping(path = "{eventID}/ventes", method = RequestMethod.GET)
    public List<Vente> getVentesFromEvent(@PathVariable Long eventID){
        return ventes.findVentesOfEvent(eventID);
    }

    @RequestMapping(path = "/{eventID}/add-catalogue/{catalogueID}", method = RequestMethod.POST)
    public boolean addCatalogueToEvent(@PathVariable Long eventID, @PathVariable Long catalogueID){
        if(!catalogues.isExisting(catalogueID)){
            return false;
        }
        else{
            Catalogue catalogue = catalogues.FindCatalogueByID(catalogueID);
            events.addCatalogueToEvent(catalogue, eventID);
            return true;
        }
    }

    @RequestMapping(path = "", method = RequestMethod.POST )
    public boolean addVenteToEvent(@PathVariable Long eventID, @PathVariable Long venteID){

        if(!ventes.isExisting(venteID)){
            return false;
        }
        else{
            Vente vente = ventes.findVenteById(venteID);
            events.addVenteToEvent(vente, eventID);
            return true;
        }
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public boolean setDataOnEvent(@RequestBody Event event){

        if(events.isExisting(event.getId())){
            events.saveEvent(event);
            return true;
        }
        else{
            return false;
        }
    }

    @RequestMapping(path = "/{eventID}", method = RequestMethod.DELETE)
    public boolean deleteEvent(@PathVariable Long eventID){

        if (events.isExisting(eventID)){
            events.deleteEvent(eventID);
            return true;
        }
        else{
            return false;
        }
    }

}
